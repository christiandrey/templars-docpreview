import paperSizes from './paper-sizes';

const constants = {
  paperSizes,
};

export default constants;
