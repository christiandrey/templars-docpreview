import format from 'date-fns/format';

export function isNil<T>(value: T): boolean {
  return typeof value === "undefined" || value === null;
}

export function isNotNil<T>(value: T): boolean {
  return !isNil(value);
}

export function getPlaceholderImage(width: number, height?: number) {
  return `https://dummyimage.com/${width}x${height || width}/326FF3/FFFFFF`;
}

export function clamp(value: number, min: number, max: number) {
  return Math.min(Math.max(value, min), max);
}

export function formatDate(dateTime: ValidDate, token = "yyyy-MM-dd"): string {
  if (isNil(dateTime)) return undefined;

  try {
    return format(new Date(dateTime), token);
  } catch (error) {
    return undefined;
  }
}

const utils = {
  isNil,
  isNotNil,
  getPlaceholderImage,
  clamp,
};

export default utils;
