import { clamp, formatDate, getPlaceholderImage, isNil, isNotNil } from './utils';

const HEADING_LINE_HEIGHTS = {
  1: 25,
  2: 23,
  3: 20,
  4: 18,
  5: 15,
  6: 13,
};

export function getMatchingNodes(
  nodes: Array<DocmakerNode>,
  match: (node: DocmakerNode) => boolean = () => true,
  matching: Array<DocmakerNode> = []
): Array<DocmakerNode> {
  for (const node of nodes) {
    if (match(node)) {
      matching.push(node);
    }

    const children = node.children;

    if (children?.length) {
      getMatchingNodes(children, match, matching);
    }
  }

  return matching;
}

export function getDocumentSlug(name: string) {
  return name
    ?.toLowerCase()
    ?.replace(/[^a-z0-9\s]/gi, "")
    .replace(/\s+/gi, "-");
}

export function getEditableNodes(data: DocmakerData): Array<DocmakerNode> {
  if (isNil(data)) {
    return [];
  }

  const { nodes, orphans } = data;
  const matching = getMatchingNodes(
    nodes,
    (o) => (o.editable && isNil(o.valueRef)) || isNotNil(o.ref)
  );
  const questions: Array<DocmakerNode> = [];

  for (const match of matching) {
    const { ref } = match;
    if (isNil(ref)) {
      questions.push(match);
      continue;
    }

    const normalizedRef = Array.isArray(ref) ? ref : [ref];
    questions.push(
      ...normalizedRef
        .filter((o) => !!orphans.find((p) => p.id === o.parent))
        .map((o) => orphans.find((p) => p.id === o.parent))
    );
  }

  return Array.from(new Set(questions));
}

export function getNodeStringValue(
  node: DocmakerNode,
  fields: DocmakerFields,
  nodes: Array<DocmakerNode>,
  defaultValue?: string
): string {
  const value = fields?.[node.id];
  let parsedValue: string;

  if (isNotNil(node.valueRef)) {
    const parentNode = getMatchingNodes(
      nodes,
      (o) => o.id === node.valueRef
    )?.[0];
    return getNodeStringValue(parentNode, fields, nodes, node.defaultValue);
  }

  switch (node.dataType) {
    case "image":
      parsedValue = isNotNil(value)
        ? value
        : getPlaceholderImage(node.width, node.height);
      break;
    case "radio":
    case "options":
      parsedValue = node.options?.find((o) => o.id === value)?.label;
      break;
    case "date":
    case "time":
      parsedValue = isNotNil(value)
        ? formatDate(value, node.dateTimeFormat ?? "MMMM d, yyyy")
        : undefined;
      break;
    default:
      parsedValue = value;
  }

  return (
    (!!parsedValue?.length ? parsedValue : undefined) ??
    defaultValue ??
    node.defaultValue
  );
}

export function getNodeVisibility(
  node: DocmakerNode,
  fields: DocmakerFields
): boolean {
  const { ref } = node;

  if (isNil(ref)) {
    return true;
  }

  const normalizedRef = Array.isArray(ref) ? ref : [ref];
  return normalizedRef.every((o) => fields[o.parent] === o.value);
}

export function getIndentationPercent(value: number): string {
  if (!value) {
    return null;
  }

  return `${value}%`;
}

export function getHeadingFontSpecs(level: number) {
  return {
    fontSize: clamp(10 + 2 * (6 - level), 10, 25),
    lineSpacing: HEADING_LINE_HEIGHTS[level],
  };
}
const docmaker = {
  getMatchingNodes,
  getEditableNodes,
  getDocumentSlug,
  getNodeStringValue,
  getIndentationPercent,
  getHeadingFontSpecs,
};

export default docmaker;
