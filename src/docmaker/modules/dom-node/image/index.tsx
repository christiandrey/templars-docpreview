import { useDocmakerFieldState } from 'docmaker/hooks';
import React, { CSSProperties, FC, useRef } from 'react';

const DocmakerDomImage: FC<DocmakerRenderedElementProps> = ({
  node,
  nodes,
  fields,
}) => {
  const state = useDocmakerFieldState(node, nodes, fields);
  const style = useRef({} as CSSProperties).current;

  return (
    <img
      alt=""
      src={state.value}
      style={{
        ...style,
        ...{
          alignSelf: "baseline",
          width: node.width,
          height: node.height,
          borderRadius: 5,
        },
      }}
    />
  );
};

export default DocmakerDomImage;
