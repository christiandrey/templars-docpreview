import React, { CSSProperties, FC, useRef } from 'react';

const DocmakerDomLeaf: FC<DocmakerRenderedElementProps> = ({node}) => {
	const style = useRef({} as CSSProperties).current;

	if (node.bold) {
		style.fontWeight = 'bold';
	}

	if (node.code) {
		style.fontFamily = 'monospace';
	}

	if (node.italic) {
		style.fontStyle = 'italic';
	}

	if (node.underline) {
		style.textDecoration = 'underline';
	}

	if (node.strikethrough) {
		style.textDecoration = 'line-through';
	}

	return <span style={{...style, color: node.color}}>{node.text}</span>;
};

export default DocmakerDomLeaf;
