import { useDocmakerFieldState } from 'docmaker/hooks';
import React, { FC } from 'react';

const DocmakerDomEditable: FC<DocmakerRenderedElementProps> = ({
  node,
  nodes,
  fields,
}) => {
  const state = useDocmakerFieldState(node, nodes, fields);

  return (
    <span
      style={{
        fontSize: "82%",
        backgroundColor: "#EAEEF7",
        borderRadius: 5,
        paddingLeft: 4,
        paddingRight: 4,
        paddingTop: 2,
        paddingBottom: 2,
      }}
    >
      {state.value}
    </span>
  );
};

export default DocmakerDomEditable;
