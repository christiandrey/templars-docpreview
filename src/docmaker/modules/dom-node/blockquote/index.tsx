import { getIndentationPercent } from 'docmaker/plugins';
import { isNotNil } from 'docmaker/plugins/utils';
import React, { CSSProperties, FC, useRef } from 'react';

const DocmakerDomBlockquote: FC<DocmakerRenderedElementProps> = ({
  node,
  children,
}) => {
  const style = useRef({} as CSSProperties).current;

  if (isNotNil(node.alignment)) {
    style.textAlign = node.alignment;
  }

  if (isNotNil(node.indentation)) {
    style.paddingLeft = getIndentationPercent(node.indentation);
  }

  return (
    <p
      style={{
        ...style,
        marginTop: 12,
        marginBottom: 12,
        paddingLeft: 24,
        borderLeftWidth: 2,
        borderLeftStyle: "solid",
        borderLeftColor: "#E6E6E6",
      }}
    >
      {children}
    </p>
  );
};

export default DocmakerDomBlockquote;
