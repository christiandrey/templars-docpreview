import { getHeadingFontSpecs, getIndentationPercent } from 'docmaker/plugins';
import { isNotNil } from 'docmaker/plugins/utils';
import React, { CSSProperties, FC, useRef } from 'react';

const DocmakerDomHeading: FC<
  DocmakerRenderedElementProps & { level: number }
> = ({ node, level, children }) => {
  const style = useRef({} as CSSProperties).current;
  const fontSpecs = useRef(getHeadingFontSpecs(level)).current;

  if (isNotNil(node.alignment)) {
    style.textAlign = node.alignment;
  }

  if (isNotNil(node.indentation)) {
    style.paddingLeft = getIndentationPercent(node.indentation);
  }

  return (
    <div style={{ ...style, ...fontSpecs, marginBottom: 8 }}>{children}</div>
  );
};

DocmakerDomHeading.defaultProps = {
  level: 1,
};

export default DocmakerDomHeading;
