import { isNotNil } from 'docmaker/plugins/utils';
import React, { Children, cloneElement, CSSProperties, FC, ReactElement, useRef } from 'react';

const DocmakerDomBulletedList: FC<DocmakerRenderedElementProps> = ({
  node,
  children,
}) => {
  const style = useRef({} as CSSProperties).current;

  if (isNotNil(node.alignment)) {
    style.textAlign = node.alignment;
  }

  return (
    <div style={style}>
      {Children.map(children, (child: ReactElement) =>
        cloneElement(child, {
          children: [
            <span style={{ marginRight: 6 }}>•</span>,
            ...Children.toArray(child.props.children),
          ],
        })
      )}
    </div>
  );
};

export default DocmakerDomBulletedList;
