import { getIndentationPercent } from 'docmaker/plugins';
import { isNotNil } from 'docmaker/plugins/utils';
import React, { CSSProperties, FC, useRef } from 'react';

const DocmakerDomParagraph: FC<DocmakerRenderedElementProps> = ({
  node,
  children,
}) => {
  const style = useRef({ minHeight: 18 } as CSSProperties).current;

  if (isNotNil(node.alignment)) {
    style.textAlign = node.alignment;
  }

  if (isNotNil(node.indentation)) {
    style.paddingLeft = getIndentationPercent(node.indentation);
  }

  return <div style={{ ...style }}>{children}</div>;
};

export default DocmakerDomParagraph;
