import React, { CSSProperties, FC, useRef } from 'react';

const DocmakerDomLink: FC<DocmakerRenderedElementProps> = ({node, children}) => {
	const style = useRef({} as CSSProperties).current;

	return (
		<a href={node.url} style={style}>
			{children}
		</a>
	);
};

export default DocmakerDomLink;
