import React, { FC } from 'react';

const DocmakerDomCode: FC<DocmakerRenderedElementProps> = ({node, children}) => {
	return (
		<div
			style={{
				marginTop: 6,
				marginBottom: 6,
			}}>
			<span style={{fontFamily: 'monospace'}}>{children}</span>
		</div>
	);
};

export default DocmakerDomCode;
