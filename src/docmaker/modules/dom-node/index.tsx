import { useDocmakerFieldState } from 'docmaker/hooks';
import React, { FC, useCallback } from 'react';

import DocmakerDomBlockquote from './blockquote';
import DocmakerDomBulletedList from './bulleted-list';
import DocmakerDomCode from './code';
import DocmakerDomEditable from './editable';
import DocmakerDomHeading from './heading';
import DocmakerDomImage from './image';
import DocmakerDomLeaf from './leaf';
import DocmakerDomLink from './link';
import DocmakerDomListItem from './list-item';
import DocmakerDomNumberedList from './numbered-list';
import DocmakerDomParagraph from './paragraph';

const DomNode: FC<DocmakerRenderedElementProps> = ({ node, nodes, fields }) => {
  const state = useDocmakerFieldState(node, nodes, fields);

  const renderChildren = useCallback(() => {
    return node.children?.map((o, i) => (
      <DomNode node={o} nodes={nodes} fields={fields} key={i} />
    ));
  }, [fields, node.children, nodes]);

  if (!state.visible) {
    return null;
  }

  switch (node.type) {
    default:
      return (
        <DocmakerDomLeaf node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomLeaf>
      );
    case "bulleted-list":
      return (
        <DocmakerDomBulletedList node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomBulletedList>
      );
    case "code":
      return (
        <DocmakerDomCode node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomCode>
      );
    case "editable":
      return node.dataType === "image" ? (
        <DocmakerDomImage node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomImage>
      ) : (
        <DocmakerDomEditable node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomEditable>
      );
    case "heading-one":
      return (
        <DocmakerDomHeading level={1} node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomHeading>
      );
    case "heading-two":
      return (
        <DocmakerDomHeading level={2} node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomHeading>
      );
    case "heading-three":
      return (
        <DocmakerDomHeading level={3} node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomHeading>
      );
    case "heading-four":
      return (
        <DocmakerDomHeading level={4} node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomHeading>
      );
    case "heading-five":
      return (
        <DocmakerDomHeading level={5} node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomHeading>
      );
    case "heading-six":
      return (
        <DocmakerDomHeading level={6} node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomHeading>
      );
    case "image":
      return (
        <DocmakerDomImage node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomImage>
      );
    case "link":
      return (
        <DocmakerDomLink node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomLink>
      );
    case "list-item":
      return (
        <DocmakerDomListItem node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomListItem>
      );
    case "numbered-list":
      return (
        <DocmakerDomNumberedList node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomNumberedList>
      );
    case "paragraph":
      return (
        <DocmakerDomParagraph node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomParagraph>
      );
    case "quote":
      return (
        <DocmakerDomBlockquote node={node} nodes={nodes} fields={fields}>
          {renderChildren()}
        </DocmakerDomBlockquote>
      );
  }
};

export default DomNode;
