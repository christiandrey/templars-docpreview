import { getIndentationPercent } from 'docmaker/plugins';
import { isNotNil } from 'docmaker/plugins/utils';
import React, { CSSProperties, FC, useRef } from 'react';

const DocmakerDomListItem: FC<DocmakerRenderedElementProps> = ({
  node,
  children,
}) => {
  const style = useRef({} as CSSProperties).current;

  if (isNotNil(node.indentation)) {
    style.paddingLeft = getIndentationPercent(node.indentation);
  }

  return <span style={style}>{children}</span>;
};

export default DocmakerDomListItem;
