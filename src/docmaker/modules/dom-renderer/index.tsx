import constants from 'docmaker/constants';
import { getEditableNodes } from 'docmaker/plugins';
import React, { FC, useMemo } from 'react';

import DomNode from '../dom-node';

type DomRendererProps = {
  template: DocmakerData;
  fields: DocmakerFields;
};

const DomRenderer: FC<DomRendererProps> = ({ template, fields }) => {
  const nodes = template.nodes;
  const editableNodes = useMemo(() => getEditableNodes(template), [template]);

  return (
    <div
      aria-roledescription="Document"
      style={{
        backgroundColor: "white",
        overflow: "auto",
        width: constants.paperSizes.a4.width,
        height: constants.paperSizes.a4.height,
      }}
    >
      <div
        aria-roledescription="Page"
        style={{
          boxSizing: "border-box",
          paddingTop: 35,
          paddingBottom: 65,
          paddingLeft: 35,
          paddingRight: 35,
          width: constants.paperSizes.a4.width,
        }}
      >
        {nodes?.map((o, i) => (
          <DomNode node={o} nodes={editableNodes} fields={fields} key={i} />
        ))}
      </div>
    </div>
  );
};

export default DomRenderer;
