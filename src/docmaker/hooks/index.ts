import { getNodeStringValue, getNodeVisibility } from 'docmaker/plugins';
import { useMemo } from 'react';

export function useDocmakerFieldState(
  node: DocmakerNode,
  nodes: Array<DocmakerNode>,
  fields: DocmakerFields
) {
  const value = useMemo(() => getNodeStringValue(node, fields, nodes), [
    node,
    fields,
    nodes,
  ]);
  const visible = useMemo(() => getNodeVisibility(node, fields), [
    node,
    fields,
  ]);
  const state = useMemo(() => ({ value, visible }), [value, visible]);

  return state;
}
