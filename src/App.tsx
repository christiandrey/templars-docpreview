import './App.css';

import data from 'docmaker/data';
import DomRenderer from 'docmaker/modules/dom-renderer';
import React, { useMemo } from 'react';

function App() {
  const template = useMemo(() => JSON.parse(data.template) as DocmakerData, []);
  const fields = useMemo(() => JSON.parse(data.fields) as DocmakerFields, []);

  return (
    <div className="App">
      <DomRenderer template={template} fields={fields} />
    </div>
  );
}

export default App;
