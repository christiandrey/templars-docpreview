type ValidDate = Date | string | number;

// ------------------------------------------------------------------------------------
// Docmaker types
// ------------------------------------------------------------------------------------
type DocmakerFields = Record<string, string>;

type DocmakerNodeType =
  | "bulleted-list"
  | "code"
  | "editable"
  | "heading-five"
  | "heading-four"
  | "heading-one"
  | "heading-six"
  | "heading-three"
  | "heading-two"
  | "image"
  | "link"
  | "list-item"
  | "numbered-list"
  | "paragraph"
  | "quote";

type DocmakerNodeDataType =
  | "text"
  | "options"
  | "radio"
  | "time"
  | "date"
  | "image";

type DocmakerNodeAlignment = "left" | "center" | "right" | "justify";

type DocmakerRef = {
  parent: string;
  value: string;
};

type DocmakerOption = {
  id: string;
  label: string;
};

type DocmakerLeafAttributes = Partial<{
  bold: boolean;
  code: boolean;
  italic: boolean;
  underline: boolean;
  strikethrough: boolean;
  color: string;
}>;

type DocmakerImageAttributes = {
  url?: string;
  width: number;
  height: number;
};

type DocmakerLinkAttributes = {
  url?: string;
};

type DocmakerEditableAttributes = Partial<{
  id: string;
  editable: boolean;
  dataType: DocmakerNodeDataType;
  dateTimeFormat: string;
  defaultValue: string;
  isOrphan: boolean;
  label: string;
  multiline: boolean;
  options: Array<DocmakerOption>;
  ref: DocmakerRef | Array<DocmakerRef>;
  tip: string;
  valueRef: string;
}>;

type DocmakerElementAttributes = Partial<{
  alignment: DocmakerNodeAlignment;
  indentation: number;
}>;

type DocmakerNode = {
  type?: DocmakerNodeType;
  text?: string;
  children?: Array<DocmakerNode>;
} & DocmakerElementAttributes &
  DocmakerEditableAttributes &
  DocmakerLinkAttributes &
  DocmakerImageAttributes &
  DocmakerLeafAttributes;

type DocmakerRenderedElementProps = {
  node: DocmakerNode;
  nodes: Array<DocmakerNode>;
  fields: DocmakerFields;
};

type DocmakerData = {
  title: string;
  createdAt: string;
  nodes: Array<DocmakerNode>;
  orphans: Array<DocmakerNode>;
};
